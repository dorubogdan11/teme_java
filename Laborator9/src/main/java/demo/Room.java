package demo;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="room")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name", nullable = false)
    private String roomName;

    @OneToMany(targetEntity = Track.class, fetch = FetchType.EAGER, mappedBy = "room")
    private Set<Track> tracks;

    public long getId() {
        return id;
    }

    public Room() {}

    public Room(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomName() { return roomName; }

    public Set<Track> getTracks() { return tracks; }

    public void setRoomName(String roomName) { this.roomName = roomName; }

    public void setTracks(Set<Track> tracks) { this.tracks = tracks; }

    public void addTrack(Track track) { tracks.add(track);}

    public void removeTrack(Track track) {
        tracks.remove(track);
    }

    public void update(Room room) {
        this.roomName = room.getRoomName();
    }
}
