package demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskService {
    @Autowired
    private TaskRepository repository;

    public List<TaskModel> getTasks(String title, String description, String assignedTo, TaskModel.TaskStatus status, TaskModel.TaskSeverity severity) {
        return repository.findAll().stream()
                .filter(task -> isMatch(task, title, description, assignedTo, status, severity))
                .collect(Collectors.toList());
    }
    private boolean isMatch (TaskModel task, String title, String description, String assignedTo, TaskModel.TaskStatus status, TaskModel.TaskSeverity severity) {
        return (title == null || task.getTitle().toLowerCase().startsWith(title.toLowerCase()))
                && (description == null || task.getDescription().toLowerCase().startsWith(description.toLowerCase()))
                && (assignedTo == null || task.getAssignedTo().toLowerCase().startsWith(assignedTo.toLowerCase()))
                && (status == null || task.getStatus().equals(status))
                && (severity == null || task.getSeverity().equals(severity));
    }

    public Optional<TaskModel> getTask(String id) {
        return repository.findById(id);
    }

    public TaskModel addTask (TaskModel task) throws IOException {
        repository.save(task);
        return task;
    }

    public boolean updateTask (String id, TaskModel task) throws IOException {
        if(repository.findById(id).isPresent()) {
            task.setId(id);
            repository.save(task);
            return true;
        } else {
            return false;
        }
    }

    public boolean patchTask (String id, TaskModel task) throws IOException {
        Optional<TaskModel> existingTask = repository.findById(id);
        if(existingTask.isPresent()) {
            existingTask.get().patch(task);
            repository.save(existingTask.get());
            return true;
        } else {
            return false;
        }
    }
    public boolean deleteTask (String id) throws IOException {
        return repository.deleteById(id);
    }

    public String getCsv(List<Map<String, Object>> mapList){
        List<String> csv = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        StringBuilder csvBuilder = new StringBuilder();

        mapList.get(0).forEach( (key, value) -> {
            builder.append(key).append(",");
        });
        csv.add(builder.substring(0, builder.toString().length() - 1));

        mapList.forEach(map -> {
            List<String> stringList = new ArrayList<>();

            map.forEach((key, value) -> stringList.add(String.valueOf(value)));
            String response = stringList.toString();
            response = response.substring(1,response.length()-1);
            csv.add(response);
        });

        csvBuilder.append(String.join("\n", csv));
        return csvBuilder.toString();
    }

    public String getXml(List<Map<String, Object>> mapList){
        List<String> xml = new ArrayList<>();
        StringBuilder xmlBuilder = new StringBuilder();

        mapList.forEach(map -> {
            List<String> stringList = new ArrayList<>();

            map.forEach( (key, value) -> {
                stringList.add("<" + key + ">" + value + "</" + key + ">");
            });
            String value = stringList.toString().replaceAll(",","");
            value = value.substring(1,value.length()-1);
            xml.add("<task>" + value + "</task>");
        });

        xmlBuilder.append("<tasks>");
        xmlBuilder.append(String.join("", xml));
        xmlBuilder.append("</tasks>");
        return xmlBuilder.toString();
    }
}
